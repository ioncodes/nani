#include <nani/ui.hpp>
#include <nani/color.hpp>

using namespace nani;

void test_ui()
{
    const auto draw = []()
    {
        ui::text("Example text 1", 10, 10, 20, colors::lightgray);
        ui::text("Example text 2", 10, 30, 20, colors::lightgray);
    };

    const auto renderer = ui::init(800, 600, "lol");

    while (!renderer->should_close())
    {
        renderer->clear(colors::darkgray);
        renderer->draw(draw);
    }
}

int main()
{
    test_ui();

    return 0;
}