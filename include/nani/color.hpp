#pragma once

#include <cinttypes>

namespace nani
{
    struct color
    {
        uint8_t r;
        uint8_t g;
        uint8_t b;
        uint8_t a;

        color(uint8_t r, uint8_t g, uint8_t b, uint8_t a) : r(r), g(g), b(b), a(a) {}
    };

    struct colors
    {
        static inline nani::color lightgray = nani::color { 200, 200, 200, 255 };
        static inline nani::color darkgray = nani::color { 80, 80, 80, 255 };
    };
}