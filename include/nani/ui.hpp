#pragma once

#include <raylib.h>
#include <string>
#include <memory>
#include <functional>
#include <utility>

#include "color.hpp"

namespace nani::ui
{
    Color convert_color(const nani::color& color)
    {
        return Color { color.r, color.g, color.b, color.a };
    }

    struct renderer
    {
        ~renderer()
        {
            CloseWindow();
        }

        static void draw(const std::function<void(void)>& drawer)
        {
            BeginDrawing();
            drawer();
            EndDrawing();
        }

        static void clear(const nani::color& background_color)
        {
            const auto color = ui::convert_color(background_color);
            ClearBackground(color);
        }

        static bool should_close()
        {
            return WindowShouldClose();
        }
    };

    void text(const std::string& text, const int x, const int y, const int font_size, const nani::color& font_color)
    {
        const auto color = ui::convert_color(font_color);
        DrawText(text.data(), x, y, font_size, color);
    }

    std::unique_ptr<renderer> init(const int width, const int height, const std::string& title, const int fps = 60)
    {
        InitWindow(width, height, title.data());
        SetTargetFPS(fps);

        return std::make_unique<renderer>();
    }
}